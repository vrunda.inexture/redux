import './App.css';
import Counter from './components/Counter';
import ItemList from './components/ItemList';

function App() {
  return (
    <div className='App'>
      <Counter />
      <hr />
      <br />
      <ItemList />
    </div>
  );
}

export default App;
