import React from 'react';
import { connect } from 'react-redux';
import { decrement, increment } from '../actions/demoActions';

const Counter = ({ increment, decrement, counter }) => {
  return (
    <div>
      <h1>
        Counter: {counter}
        <br />
      </h1>
      <button onClick={increment}>Increment +</button>{' '}
      <button onClick={decrement}>Decrement -</button>
    </div>
  );
};

const mapStateToProps = (state) => ({
  counter: state.demo.counter,
});

export default connect(mapStateToProps, { increment, decrement })(Counter);
