import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addItem } from '../actions/listAction';

const ItemList = ({ items, addItem }) => {
  const [field, setField] = useState({
    name: '',
    price: '',
  });

  const handleChange = (e) => {
    setField({ ...field, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    addItem({ id: items.length + 1, ...field });
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        Name :
        <input
          type='text'
          name='name'
          value={field.name}
          onChange={handleChange}
        />{' '}
        Price:{' '}
        <input
          type='number'
          name='price'
          value={field.price}
          onChange={handleChange}
        />
        <button type='submit'>Submit</button>
      </form>
      <h4>List Items:</h4>
      <ul>
        {items.map((item) => (
          <li key={item.id}>
            {item.name} (${item.price})
          </li>
        ))}
      </ul>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.list.items,
});

const mapDispatchToProps = (dispatch) => ({
  addItem: (obj) => dispatch(addItem(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);
