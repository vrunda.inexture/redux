import { ADD_ITEM } from './types';

export const addItem = (item) => {
  console.log(item);
  return {
    type: ADD_ITEM,
    payload: item,
  };
};
