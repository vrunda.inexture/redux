import { combineReducers } from 'redux';
import DemoReducer from './DemoReducer';
import ListReducer from './ListReducer';

export default combineReducers({
  demo: DemoReducer,
  list: ListReducer,
});
