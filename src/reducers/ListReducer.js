import { ADD_ITEM } from '../actions/types';

const initialState = {
  items: [
    {
      id: 1,
      name: 'Popcorn',
      price: 250,
    },
    {
      id: 2,
      name: 'Ice-cream',
      price: 150,
    },
  ],
};

const DemoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      console.log(action.payload);
      return {
        items: [action.payload, ...state.items],
      };

    default:
      return state;
  }
};

export default DemoReducer;
